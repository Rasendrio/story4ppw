from django.shortcuts import render, redirect
from .models import Schedule as jadwal
from .forms import SchedForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def home(request):
    return render(request, 'home.html')

def hobi(request):
    return render(request, 'hobi.html')

def hubungi(request):
    return render(request, 'hubungi.html')

def pendidikan(request):
    return render(request, 'pendidikan.html')

def Join(request) :
    if request.method == "POST" :
        form = SchedForm(request.POST)
        if form.is_valid() :
            sched = jadwal()
            sched.Mata_Kuliah = form.cleaned_data['Mata_Kuliah']
            sched.Dosen = form.cleaned_data['Dosen']
            sched.Kelas = form.cleaned_data['Kelas']
            sched.Tahun = form.cleaned_data['Tahun']
            sched.SKS = form.cleaned_data['SKS']
            sched.save()
        return redirect('/Schedule')
    else :
        sched = jadwal.objects.all()
        form = SchedForm()
        response = {'sched' : sched , 'form' : form}
        return render(request, 'schedule.html', response)

def sched_delete(request, pk) :
    if request.method == "POST" :
        form = SchedForm(request.POST)
        if form.is_valid() :
            sched = jadwal()
            sched.Mata_Kuliah = form.cleaned_data['Mata_Kuliah']
            sched.Dosen = form.cleaned_data['Dosen']
            sched.Kelas = form.cleaned_data['Kelas']
            sched.Tahun = form.cleaned_data['Tahun']
            sched.SKS = form.cleaned_data['SKS']
            sched.save()
        return redirect('/Schedule')
    else :
        sched = jadwal.objects.filter(pk=pk).delete()
        data = jadwal.objects.all()
        form = SchedForm()
        response = {'sched' : data , 'form' : form}
        return render(request, 'schedule.html', response)

def signup(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = User.objects.create_user(username = username , password = password)
        return redirect('/signup')
    else :
        return render(request,'signup.html')

def signin(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username = username , password = password)
        if user != None :
            login(request,user)
            
            return redirect('/home')
        else :
            return redirect('/signin')
    else :
        return render(request,'signin.html')
    
def logout1(request):
    logout(request)
    return redirect('/logout')

def books(request):
    return render(request,'caribuku.html')