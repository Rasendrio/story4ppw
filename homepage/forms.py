from django import forms

class SchedForm(forms.Form) :
    Mata_Kuliah = forms.CharField(widget = forms.TextInput(attrs = {
        'class' : 'form-control' ,
        'placeholder' : 'Mata_Kuliah' ,
        'type' : 'text',
        'required' : True ,
    }))

    Dosen = forms.CharField(widget = forms.TextInput(attrs = {
        'class' : 'form-control' ,
        'placeholder' : 'Dosen' ,
        'type' : 'text',
        'required' : True ,
    }))
    
    Kelas = forms.CharField(widget = forms.TextInput(attrs = {
        'class' : 'form-control' ,
        'placeholder' : 'Kelas' ,
        'type' : 'text',
        'required' : True ,
    }))

    Tahun = forms.IntegerField(widget = forms.NumberInput(attrs = {
        'class' : 'form-control' ,
        'placeholder' : 'yyyy' ,
        'type' : 'number',
        'required' : True ,
    }))

    SKS = forms.IntegerField(widget = forms.NumberInput(attrs = {
        'class' : 'form-control' ,
        'placeholder' : 'SKS' ,
        'type' : 'number',
        'required' : True ,
    }))