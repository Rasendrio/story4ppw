from django.db import models

# Create your models here.
class Schedule(models.Model) :
    Mata_Kuliah = models.CharField(max_length = 50,default='')
    Dosen = models.CharField(max_length = 50,default='')
    Kelas = models.CharField(max_length = 50,default='')
    Tahun = models.IntegerField()
    SKS = models.IntegerField()