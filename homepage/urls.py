from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('hobi/', views.hobi, name='hobi'),
    path('hubungi/', views.hubungi, name = 'hubungi'),
    path('pendidikan/', views.pendidikan, name = 'pendidikan'),
    path('Schedule/', views.Join, name = 'schedule') ,
    path('<int:pk>', views.sched_delete, name = 'delete'),
    path('signup/',views.signup, name ='signup'),
    path('signin/',views.signin, name ='signin'),
    path('logout/',views.logout1, name ='logout'),
    path('caribuku/',views.books,name='books'),
]